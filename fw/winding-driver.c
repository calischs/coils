#include <avr/io.h>
#include <stdio.h>
#include <avr/sleep.h>
#include <util/delay.h>

#define LED1_mask PIN2_bm
#define LED2_mask PIN3_bm

//#define wire_d 202000 //32 AWG, magnet wire diameter in nanometers
const uint64_t wire_d = 205000; //33 AWG + coating = 8.1 mils, magnet wire diameter in nanometers
//#define wire_d 160000 //34 AWG, magnet wire diameter in nanometers
const uint64_t n_ticks_per_rev = 400; //number of encoder edges per revolution
const uint64_t nm_per_step = 484; //quarter steps for now
//const uint64_t nm_per_step = 400; //quarter steps for now
const int64_t center_width = 1000000; //1mm post width
const uint64_t n_layers = 10; //how many layers total
const uint64_t n_wraps_per_layer = 9; //how many coil wraps per layer before reversing
//const uint32_t n_ticks_per_layer = n_ticks_per_rev * n_wraps_per_layer; //how many encoder ticks per layer
uint64_t n_ticks_per_layer = 0; //how many encoder ticks per layer

//state variables
char still_winding = 1; //are we still winding?
uint64_t count = 0;
uint64_t n_layers_done = 0;
uint64_t n_wraps_this_layer = 0;
uint64_t n_ticks_this_rev = 0;
int64_t x_desired = 0; //where we want the tip, measured in nanometers from the center position
int64_t x_current = 0; //where we are currently, measured in nanometers from the center position


void update_desired_x_left(){
	//function to implement wraps and layering.
  //count = ((uint32_t)TCC1.CNT << 16) | TCC0.CNT;
  count = TCC0.CNT;
  n_layers_done = count / n_ticks_per_layer;
  if (n_layers_done >= n_layers){
    still_winding = 0; //done winding
  } else {
    n_wraps_this_layer = (count % n_ticks_per_layer) / n_ticks_per_rev;
    n_ticks_this_rev = (count % n_ticks_per_rev);
    x_desired = n_wraps_this_layer * wire_d + (n_ticks_this_rev * wire_d) / n_ticks_per_rev;
    if (n_layers_done % 2 == 1){ 
      x_desired = n_wraps_per_layer*wire_d - x_desired; //flip odd layer directions
    } 
  }
  x_desired = -center_width/2 - x_desired;
}
void update_desired_x_right(){
  //function to implement wraps and layering.
  //count = ((uint32_t)TCC1.CNT << 16) | TCC0.CNT;
  count = TCC0.CNT;
  n_layers_done = count / n_ticks_per_layer;
  if (n_layers_done == 0 ){ //count down trick
    still_winding = 0; //done winding
  } else {
    n_wraps_this_layer = (count % n_ticks_per_layer) / n_ticks_per_rev;
    n_ticks_this_rev = (count % n_ticks_per_rev);
    x_desired = n_wraps_this_layer * wire_d + (n_ticks_this_rev * wire_d) / n_ticks_per_rev;
    if (n_layers_done % 2 == 1){ 
      x_desired = n_wraps_per_layer*wire_d - x_desired; //flip odd layer directions
    } 
  }
  x_desired = center_width/2 + x_desired;
}

void step_forward(){
  PORTD.OUTCLR = PIN6_bm; //dir forward
  PORTC.OUTCLR = PIN5_bm; //dir forward
  PORTD.OUTSET = PIN5_bm; //step up
  PORTC.OUTSET = PIN4_bm; //step up
  //PORTE.OUTSET = LED1_mask; //light led
  _delay_us(10);
  PORTD.OUTCLR = PIN5_bm; //step down
  PORTC.OUTCLR = PIN4_bm; //step down
  x_current += nm_per_step; //update current x
  //PORTE.OUTCLR = LED1_mask; //dark led
}
void step_backward(){
  PORTD.OUTSET = PIN6_bm; //dir backward
  PORTC.OUTSET = PIN5_bm; //dir backward
  PORTD.OUTSET = PIN5_bm; //step up
  PORTC.OUTSET = PIN4_bm; //step up
  //PORTE.OUTSET = LED2_mask; //light led
  _delay_us(10);
  PORTD.OUTCLR = PIN5_bm; //step down
  PORTC.OUTCLR = PIN4_bm; //step down
  x_current -= nm_per_step; //update current x
  //PORTE.OUTCLR = LED2_mask; //dark led
}

void update_actual_x(){
    if (x_desired > x_current){
      step_forward();
    } else if (x_desired < x_current){
      step_backward();
    }
}

int main(void) {
  OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
  while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
  CCP = CCP_IOREG_gc; // enable protected register change
  CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock
   
  PORTE.DIRSET = LED1_mask | LED2_mask; //set led pins as outputs

  PORTC.DIRCLR = PIN0_bm | PIN0_bm; //set pc0 and pc1 as inputs
  PORTC.PIN0CTRL =  (PORTC.PIN0CTRL & ~PORT_ISC_gm) | PORT_ISC_LEVEL_gc; //set pc0 to be transparent for events
  PORTC.PIN1CTRL =  (PORTC.PIN1CTRL & ~PORT_ISC_gm) | PORT_ISC_LEVEL_gc; //set pc1 to be transparent for events

  EVSYS.CH0MUX = EVSYS_CHMUX_PORTC_PIN0_gc;  //let's use event channel zero for encoder input  
  EVSYS.CH0CTRL = EVSYS_QDEN_bm | EVSYS_DIGFILT_2SAMPLES_gc; //set up quadrature decoding

  // Set up TCC0 to do quadrature decoding through event channel 0
  TCC0.CTRLA = TC_CLKSEL_DIV1_gc;	
  TCC0.CTRLD = TC_EVACT_QDEC_gc | TC_EVSEL_CH0_gc;
  TCC0.PER = 0xFFFF;
  //cascade tcc0 overflow into tcc1 using event channel 1 so we get 32 bit counter of encoder pulses
  //with 400 ticks per revolution, that's ~160 turns before 16-bit overflow
  //only issue is we can't discriminate overflow from underflow, so if we step backward across zero that will be a problem
  EVSYS.CH1MUX = EVSYS_CHMUX_TCC0_OVF_gc;
  TCC1.CTRLA = TC_CLKSEL_EVCH1_gc;
  TCC1.PER = 0xFFFF;

  //DAC for current control, PB3 = DAC CHannel 1
  DACB.CTRLA = DAC_CH1EN_bm | DAC_ENABLE_bm;
  DACB.CTRLB = DAC_CHSEL_DUAL_gc;
  DACB.CTRLC = DAC_REFSEL_AVCC_gc;
  //value out of 0xFFF = 4096, scaled to 3.3V.  
  //With 200mOhm sense resistor, this voltage value equals the chop current
  //DACB.CH1DATA = 620; //200mA limit
  DACB.CH1DATA = 1000; //~300mA limit

  //stepper 1
  PORTD.DIRSET = PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm | PIN7_bm; //set outputs
  PORTD.OUTSET = PIN3_bm | PIN4_bm; //quarter steps
  PORTD.OUTSET = PIN7_bm; //enable
  //stepper 2
  PORTC.DIRSET = PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm; //set outputs
  PORTC.OUTSET = PIN2_bm | PIN3_bm; //quarter steps
  PORTC.OUTSET = PIN6_bm; //enable

  //convenience calculation
  n_ticks_per_layer = n_ticks_per_rev * n_wraps_per_layer;


  //calibration, move +-2mm repeatedly
  /*while(1){
    while( x_current <  2000000){ step_forward(); _delay_ms(1);} 
    _delay_ms(1000);
    while( x_current > -2000000){ step_backward(); _delay_ms(1);} 
    _delay_ms(1000);
  }*/


  //move left to negative half center width 
  while( x_current > - center_width / 2){
    step_backward();
    _delay_ms(1);
  }


  //wind left 
  while (still_winding) {
    update_desired_x_left();
    update_actual_x();
  }

  PORTE.OUTSET = LED2_mask; //turn on led2  
  //move left to positive half center width 
  while( x_current < center_width / 2){
    step_forward();
    _delay_ms(1);
  }
  PORTE.OUTCLR = LED2_mask; //turn off led2

  //wind right 
  still_winding = 1;
  while (still_winding) {
    update_desired_x_right();
    update_actual_x();
  }
  //turn off motors
  PORTC.OUTCLR = PIN6_bm; //sleep
  PORTD.OUTCLR = PIN7_bm; //sleep
  PORTE.OUTSET = LED1_mask; //turn on led1

}

