# Precision coil winding

For making small electromagnetic actuators, winding coils can be a significant time sink.  Further, small differences in wrapping accuracy can have huge impacts on coil density, resulting in suboptimal magnetic field generation and poor thermal dissipation, besides just looking bad and having unpredictable dimensions.  Industrially, this process is performed at high rates of production at tiny scales, just look at the selection of minute wire-wound inductors on Digikey.  Achieving good results with small diameter wire is thus possible, but not readily achieved with a general purpose winding machine.

This project is an attempt at a customizable micro-traverse for very precisely laying coils onto a magnetic core.  In the application I'm working on currently, I need a pair of coils wound onto a common core with opposite handedness.  I could wind them one at a time, but the results have been cleaner and more balanced if they are wound simultaneously.  Thus, I decided to make a dual wire device.  I started by mimicking coil winding traverses I had seen for larger coils, which use pulleys to handle the wire.

<img src='img/pulleys2.jpg' height=250px>
<img src='img/pulleys.jpg' height=250px>

These pulleys ended up being too bulky to get really close to my cores and didn't guide the wire precisly enough.  I searched around for micro-coil winding machines, and saw that guide tubes are used to get very close to the winding surface.  I decided to use this approach, but I wanted interchangeable tips for different wire diameters, adjusting the wear of the wire, etc.  There are endless varieties of inexpensive dispensing tips available (e.g., from McMaster-Carr) with varying lengths, diameters, and materials but sharing a common connection type: the Luer-Lok.  This connection uses a single pitch of threads and a mating taper.  I re-printed my traverse with a connection for these tips, using a pair of dispensing tips matched to some 32 gauge wire.

<img src='img/dispenser-tips.jpg' height=300px>
<img src='img/tips.jpg' height=300px>

For the motion system, I'm practicing my flexural and exact constraint design.  Each tip rides on a tall hinge flexure, oriented so 3D printer filament traverses the flexure.  By adjusting the lengths between the lead screw nut and the flexure and the flexure and the dispensing tip, we can very easily change the resolution and travel range of the device.

<img src='img/fusion.png' width=600px>

Each hinge is actuated by a NEMA11 stepper motor sitting at the back of the device.  The stepper shaft is coupled to a 100 TPI microadjusting screw and the mating bushing is pressed into the 3D print.  These microadjusting screws are very inexpensive for the precision they provide.  I buy them from Kozak Micro.  This screw comes with a hardened ball at its end to provide a repeatable contacting surface.  I use this ball against the planar end of the stepper shaft.  Any misalignment of the bushing, which translates into a gyration of the ball, doesn't crease any axial displacement, as the ball simply travels over the planar surface.  The screw is coupled to the shaft using some thick heat shrink, which allows this radial movement, while providing a surprisingly stiff torsional coupling.  The entire mechanism is preloaded (e.g., the ball against the shaft end) by a tension spring.

<img src='img/coupling.jpg' height=400px>
<img src='img/parts.jpg' height=400px>

Here are two videos from calibrating the traverse:

<img src='img/leadscrew-spring-small.mp4' height=400px>

<img src='img/traversing-small.mp4' height=400px>


This micro-traverse is designed to be used interactively, as a user presses a foot pedal to rotate the magnetic core and wind the wire.  Because of this, the traverse trajectory needs to be synced to an unpredictable rotational trajectory.  Luckily, the coil winder we have provides a 100 pulse per revolution encoder output via the RJ45 cable.  I designed the electronics so you just unplug this cable from the macro-traverse and plug it into the micro-traverse. 

<img src='img/eagle.png' height=250px>
<img src='img/pcb.jpg' height=250px>

I trigger use a Timer/Counter on the Xmega in quadrature decoding mode to track the encoder pulses from the coil winding spindle.  The value of this timer then represents the current position, onto which we map the traverse trajectory, calculated using wire diameter, number of wraps per layer, and total number of layers.  Firmware is available in <a href='fw/'>fw</a>.

<img src='img/winding-ecore.jpg' height=350px>
<img src='img/wound-ecore.jpg' height=350px>

Here is a core with two coils wound onto it, with magnet wire terminated on a central spacer.  This termination can be tinned with a soldering iron and soldered to a circuit.

<img src='img/terminated.jpg' height=350px>



